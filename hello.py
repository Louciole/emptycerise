from sakura import Server

from os.path import abspath, dirname
PATH = dirname(abspath(__file__))


class Hello(Server):
    @Server.expose
    def index(self):
        return open(self.path + "/static/home/home.html").read()


Hello(path=PATH, configFile="/server.ini")
