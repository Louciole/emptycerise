from sakura import Server
import json
from os.path import abspath, dirname

PATH = dirname(abspath(__file__))


class Hello(Server):
    features = {"websockets": True, "errors": {404: "/static/404.html"}}

    @Server.expose
    def index(self):
        return open(self.path + "/static/home/home.html").read()

    async def handle_message(self, websocket):
        async for message in websocket:
            data = json.loads(message)

            match data["type"]:
                case _:
                    print("unknown message received", message)


serv = Hello(path=PATH, configFile="/server.ini")
